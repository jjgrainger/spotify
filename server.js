// Require modules
var express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    http = require('http'),
    server = http.Server(app),
    io = require('socket.io')(server),
    ngrok = require('ngrok'),
    config = require('./config.json');

/*
 * Setup express
 */
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


/**
 * Web Routes
 */
app.use('/', require('./app/routes/web'));

/**
 * Api Routes
 */
app.use('/api/v1/', require('./app/routes/api'));

/*
 * Start the server
 */
server.listen(config.port);

// log the localhost url
console.log("server is available at http://localhost:%s", config.port);

// open localhost with ngrok
ngrok.connect(config.port, function(err, url) {
    // log the ngrok tunnel url
    console.log("server open to the world at %s", url);
});
