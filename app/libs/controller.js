// Spotify App Controller
//
// Dependencies
var request = require('request'),
    applescript = require('applescript'),
    spotify = require('spotify-node-applescript'),
    util = require("util"),
    events = require("events");

// create an event emitter for Controller
var Controller = function() {

    // current state of the spotify app
    // initialise with defaults
    this.state = {
        track: false,
        volume: 0,
        state: "stopped",
        position: 0,
        repeating: false,
        shuffling: false
    };

    // interval id for sync
    this.interval = null;

    events.EventEmitter.call(this);
};

util.inherits(Controller, events.EventEmitter);


// start the spotify controller
Controller.prototype.start = function(callback) {

    var self = this;

    // check spotify is running
    spotify.isRunning(function(err, running) {

        // if spotify is not open
        if( ! running ) {

            // use applescript to open the app
            applescript.execString('tell application "Spotify" to activate', function(err, rtn) {
                if (err) {
                    // Something went wrong!
                    callback('Spotify app could not be activated', null);
                } else {
                    // initialize with callback
                    // use a small timeout to give spotify app
                    // a chance to load
                    setTimeout(function() {
                        self.initialize(callback);
                    }, 100);
                }
            });

        // if spotify is running
        } else {
            // initalize with callback
            self.initialize(callback);
        }
    });
};

// initialize the controller
Controller.prototype.initialize = function(callback) {

    var self = this;

    // setup the initial state
    self.status(function(err, state) {

        // start tracking the status
        self.interval = setInterval(self.sync.bind(self), 1000);

        // no track is playing
        if(err) {
            // console.log('no state', state);
            callback(err, null);
        } else {
            // console.log('state is', state)
            callback(null, state);
        }
    });
};

// regularly sync status with app to provide events
Controller.prototype.sync = function() {

    var self = this;

    self.status(function(err, state) {

        // if the track has changed
        if(state.track !== self.state.track) {

            // track can be a local file sometimes

            if(state.track) {
                self.track(state.track.split(':')[2], function(err, track) {
                    if(err) {
                        // log it
                    } else {
                        self.emit('change', track);
                    }
                });
            }
        }

        // if the volume has changed
        if(self.state.volume !== state.volume) {
            // self.emit('volume', state.volume);
        }

        if(state.state !== self.state.state) {
            // state has changed pause, stopped, playing
            // self.emit('state', state.state);
        }

        // set previous state
        self.state = state;
    });
};


// Get the current state of the Spotify App
Controller.prototype.status = function(callback) {

    var self = this;

    applescript.execFile(__dirname + '/scripts/state.applescript', function(err, state) {
        if(err) {
            if(callback) {
                callback(err, null);
            }
        } else {
            if(callback) {
                callback(err, JSON.parse(state));
            }
        }
    });
};

// get the current playing track
Controller.prototype.now = function(callback) {

    var self = this;

    this.status(function(err, state) {
        if(err) {
            if(callback) {
                callback(err, null);
            }
        } else {
            if(state.track) {
                self.track(state.track.split(':')[2], callback);
            } else {
                if(callback) {
                    callback('No track currently playing', null);
                }
            }
        }
    });
};

// get track info from Spotify Web API
Controller.prototype.track = function(id, callback) {

    var self = this;

    request({
        url: "https://api.spotify.com/v1/tracks/" + id,
    }, function(err, res, data) {
        if(err) {

            if(callback) {
                callback(err, null);
            }
        } else {
            var data = JSON.parse(data);

            if(callback) {
                callback(null, data);
            }
        }
    });
};

// search for a track on the spotify web api
Controller.prototype.search = function(query, callback) {
    request({
        url: "https://api.spotify.com/v1/search",
        qs: {
            type: 'track',
            q: query
        }
    }, function(err, res, data) {
        var data = JSON.parse(data);
        if(err) {
            callback(err, null);
        } else if(data.error) {
            callback(data.error, null);
        } else {
            callback(err, data);
        }
    });
};

// play a specific track from search
Controller.prototype.play = function(track, context, cb) {

    var self = this;

    if(context) {
        spotify.playTrackInContext(track, context, function() {
            // Track is playing in context of an album/playlist
            self.track(track.split(':')[2], cb);
        });
    } else {
        spotify.playTrack(track, function() {
            // track is playing
            self.track(track.split(':')[2], cb);
        });
    }
};

// play pause and resume
Controller.prototype.pause = function() {};
Controller.prototype.resume = function() {};
Controller.prototype.stop = function() {};

// set the volume
Controller.prototype.volume = function() {};

// go to track position
Controller.prototype.seek = function() {};

// repeat
Controller.prototype.repeat = function() {};
// shuffle
Controller.prototype.shuffle = function() {};


// quit the application
Controller.prototype.quit = function() {};

module.exports = new Controller;

