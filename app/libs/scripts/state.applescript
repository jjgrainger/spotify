tell application "Spotify"

    set ctrack to current track
    set cvolume to sound volume as integer
    set cstate to player state
    set cpos to player position
    set crepeat to repeating
    set cshuffle to shuffling

    if ctrack is equal to missing value then
        set ctrack to false
    else
        set ctrack to "\"" & current track's id & "\""
    end if

    if cpos is equal to missing value then
        set cpos to 0 as integer
    end if

    set cstatus to "{"
    set cstatus to cstatus & "\"track\": " & ctrack
    set cstatus to cstatus & ", \"volume\": " & cvolume
    set cstatus to cstatus & ", \"state\": \"" & cstate & "\""
    set cstatus to cstatus & ", \"position\": " & cpos
    set cstatus to cstatus & ", \"repeating\": " & crepeat
    set cstatus to cstatus & ", \"shuffling\": " & cshuffle
    set cstatus to cstatus & "}"

    return cstatus
end tell
