var express = require('express'),
    router = express.Router(),
    controller = require('./../libs/controller.js'),
    spotify = new controller();

router.get('/', function(req, res) {
    spotify.now(function(err, track) {
        res.json(track);
    });
});

module.exports = router;
