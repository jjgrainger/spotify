var spotify = require('./app/libs/controller.js');
// var spotify = new controller();

// start spotify
spotify.start(function(err, state) {

    // add event listeners
    // cant use track, use trackChange or something, emitted by sync
    spotify.on('change', function(track) {
        console.log('Playing:', track.name, 'by', track.artists[0].name);
    });

    // run commands
    // spotify.search('artist:warpaint track:biggy', function(err, results) {

    //     // get the first track from the results
    //     var track = results.tracks.items.shift();

    //     // want to play a track with track uri and context uri
    //     spotify.play(track.uri, track.album.uri, function(err, track) {
    //         // console.log('now playing:', track.name, 'by', track.artists[0].name);
    //     });

    // });

});
